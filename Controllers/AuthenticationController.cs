﻿using ASPIdentitycore.api.Authentication;
using ASPIdentitycore.api.MOdels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ASPIdentitycore.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
       
        private readonly RoleManager<IdentityRole> _rolemanager;
        private readonly IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> _usermanager;

        public AuthenticationController(UserManager<ApplicationUser> usermanager,RoleManager<IdentityRole> rolemanager,IConfiguration configuration)
        {
            _usermanager = usermanager;
            _rolemanager = rolemanager;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] registerModel model)
        {
            var UserExist = await _usermanager.FindByNameAsync(model.UserName);
            if(UserExist != null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError ,new Response { Status = "Error", Message = "User already exist" });//custom response

            }
            ApplicationUser user = new ApplicationUser()
            {
                UserName = model.UserName,
                SecurityStamp = Guid.NewGuid().ToString(),
                Email = model.Email,
            };
            var result = await _usermanager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return  StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User Creation Failed" });
            }
            return Ok(new Response { Status = "Sucess", Message = "user created sucesfully"});
        }

        [HttpPost("login")]

        public async Task<IActionResult> Login([FromBody] LoginModel logmodel)
        {
            var user = await _usermanager.FindByNameAsync(logmodel.UserName);
            if (user != null && await _usermanager.CheckPasswordAsync(user,logmodel.Password))
            {
                var userRoles = await _usermanager.GetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
                };
                foreach(var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role,userRole));
                }
                var authSigninKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                var Token = new JwtSecurityToken(
                        issuer: _configuration["JWT:ValidIssuer"],
                        audience: _configuration["JWT:ValidAudience"],
                        expires: DateTime.Now.AddHours(3),
                        claims: authClaims,
                        signingCredentials: new SigningCredentials(authSigninKey, SecurityAlgorithms.HmacSha256)
                    );
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(Token)
                });
            }
            return Unauthorized();
        }
    }
}
